# WePr Front-end DevStack
Static front-end build tool with Gulp.sj.  Includes Twig.js, Sass, Bootstrap 4, Faker and so on. 

****

#### install

npm install

#### gulp tasks

* gulp - build templates, javascrpit, styles
* gulp serve - build templates, javascrpit, styles + run dev server
* gulp build:production - build templates, javascrpit, styles:build



#### docs

twig: https://twig.symfony.com/
faker: https://www.npmjs.com/package/gulp-faker
