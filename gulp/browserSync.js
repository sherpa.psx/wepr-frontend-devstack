var gulp          = require('gulp'),
    config        = require('../config'),
    htmlInjector  = require("bs-html-injector"),
    browserSync   = require('browser-sync');

// Static Server + watching scss files

gulp.task('serve', config.tasks.serve, function(){
  browserSync.use(htmlInjector, {
      files: "dist/*.html"
  });
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  });
  gulp.watch(config.styles.src + '**/*.scss', ['styles']);
  gulp.watch(config.scripts.src + '**/*.js', ['scripts']);
  gulp.watch(config.images.src + '*', ['images']);
  gulp.watch(config.images.dist + "*").on('change', browserSync.reload);
  gulp.watch([config.templates.src + '**/*.twig', config.templates.src + '*.json'], ['templates']);
  gulp.watch([config.templates.dist + "*.html"], htmlInjector);
});
